export const newImage = () => {
  return {
    url: '',
    scale: 100,
    offsetX: 0,
    offsetY: 0,
  };
}

export default {
  methods: {
    getImageStyle(image) {
      const styles = {
        'transform': `scale3d(${image.scale / 100}, ${image.scale / 100}, 1) translate3d(${image.offsetX}%, ${image.offsetY}%, 0)`,
        'background-repeat': 'no-repeat',
        'background-size': 'contain',
        'background-position': 'center center',
        'position': 'absolute',
        'top': 0,
        'left': 0,
        'bottom': 0,
        'right': 0,
      };
      
      if (image.url.length) styles['background-image'] = `url('${image.url}')`;

      return styles;
    },
    resetLogo() {
      return newImage();
    },
  },
}
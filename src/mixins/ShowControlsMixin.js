export default {
  data() {
    return {
      showControls: false,
      keysDown: {
        ctrl: false,
        space: false,
      }
    }
  },
  created() {
    window.addEventListener('keydown', (event) => {
      if (event.key === 'Control') {
        this.keysDown.ctrl = true;
      }
      if (event.key === ' ') {
        this.keysDown.space = true;
      }
      if (this.keysDown.ctrl && this.keysDown.space) {
        this.showControls = !this.showControls;
      }
    });

    window.addEventListener('keyup', (event) => {
      if (event.key === 'Control') {
        this.keysDown.ctrl = false;
      }
      if (event.key === ' ') {
        this.keysDown.space = false;
      }
    });
  }
}
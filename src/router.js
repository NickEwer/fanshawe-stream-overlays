import Vue from 'vue';
import VueRouter from 'vue-router';
import Calendar from './interfaces/Calendar.vue';
import CasterBanner from './interfaces/CasterBanner.vue';
import PlayerRoster from './interfaces/PlayerRoster.vue';
import Scoreboard from './interfaces/Scoreboard.vue';
import Home from './pages/Home.vue';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/calendar',
      component: Calendar,
      name: 'Calender'
    }, 
    {
      path: '/scoreboard',
      component: Scoreboard,
      name: 'Scoreboard'
    },
    {
      path: '/casterBanner',
      component: CasterBanner,
      name: 'CasterBanner'
    },
    {
      path: '/playerRoster',
      component: PlayerRoster,
      name: 'PlayerRoster'
    },
    {
      path: '/',
      component: Home,
      name: 'Home'
    },
    {
      path: '/*',
      beforeEnter(to, from, next) {
        next('/')
      }
    },
  ],
});
const getCalendar = async (calendarId) => {
  return new Promise((resolve, reject) => {

    var today = new Date(); //today date
    try {
      gapi.client.load('calendar', 'v3', function () {
        var request = gapi.client.calendar.events.list({
          'calendarId' : calendarId,
          'timeZone' : 'America/Toronto', 
          'singleEvents': true, 
          'timeMin': today.toISOString(), // gathers only events not happened yet
          'maxResults': 50, 
          'orderBy': 'startTime'});
        request.execute(function (resp) {
          return resolve(resp);
        });
      });
    }
    catch(e) {
      return reject(e);
    }
  })
}

export default getCalendar;